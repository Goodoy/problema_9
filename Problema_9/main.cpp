/* Programa que recibe un tamaño y divide el string en grupos de n numeros en su orden y luego toma todos los grupos
   y los suma, luego los retona*/

#include <iostream>
#include <math.h>

using namespace std;

int tamArreglo(char numero[]); //toma un arreglo y retorna su tamaño
int charToInt(char numero[]); // Convierte el char en int recibiendo un char[] y refereciando un int[] creado en main
int sumaFracciones(int nParts, int num, int div); //Recibe el numero de partes, el numero y la potencia o tamaño,
                                                  // y hace la division y suma.
int main()
{
	
    int num=0,div=0,numParts=0,suma=0;  //inicializa las variables
    char numero[128];  //Inicia el arreglo numero
	do{
		cout << "Escriba el tamanio del separador: ";
        cin >> num;
    }while(num<=0);  //Recibe la cantidad de numeros que va en cada grupo
	
    cout << "Ingrese el numero: ";
    cin >> numero;  //Recibe el numero en un string
	
    div = pow(10,num); //Almacena el tamaño del numero Ejm 100--->3
	
    numParts= ((tamArreglo(numero)/num)+1); //Se toma el numero de partes necesarias para guardar los numeros
    suma = sumaFracciones(numParts,charToInt(numero),div); //Se realiza toda la operacion de separar y sumar
	
    cout << "Original: "<<numero<<endl; //Se imprime el orginal
    cout << "Suma: "<<suma; //Se imprime la suma de todos sus grupos
	
	return 0;
}
int tamArreglo(char numero[]){
    long int con=0;
    while(numero[con]!=0){  //Itera hasta encontrar el valor nulo y termina retornando el numero de posiciones
        con++;              //Que tienen contenido.
	}
    return con;
}

int sumaFracciones(int nParts, int num, int div){
    int acum=0,a=num,b=0; //Variables
	
    for(int i=0;i<nParts;i++){  //Itera por el numero de partes
        b=a%div; //toma el i grupo de numeros definido por el tamaño b
        a=a/div; //Lo elimina
        acum=acum+b; //Lo suma
	}
	
	return acum;
}

int charToInt(char numero[]){
	int k=0;
    long int acum=0;
    int tam = pow(10,tamArreglo(numero)-1);  //Toma el tamaño del arreglo Ejm: 100000---> numero de 6 cifras
    while(numero[k]!=0){   //Mientras no sea 0
        acum=acum+(numero[k]-48)*tam;  //Toma el primer numero, lo convierte en int y lo multiplica por su valor
        tam/=10; //Le quita una posicion al valor
        k++; //corre a la siguiente posicion del arreglo
	}
    return acum;  //Devuelve la variable que almacena el numero
}
